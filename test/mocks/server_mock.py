import zmq
from _thread import start_new_thread
from typing import List, Tuple


class ZMQServerMock:
    uri: str = "tcp://127.0.0.1:8403"
    context = zmq.Context()
    socket = None
    running: bool = True

    response_message: dict = {}
    use_message_flag: bool = False

    received_messages: List[Tuple[bytearray, dict]] = []
    catch_messages: bool = False

    def __init__(self, uri: str):
        self.uri = uri
        self.socket = self.context.socket(zmq.ROUTER)
        self.socket.bind(self.uri)

    def __del__(self):
        self.kill()
        self.socket.close()

    def start(self):
        start_new_thread(self.run_ping_server, ())

    def kill(self):
        self.running = False

    def run_ping_server(self):
        while self.running:
            id: bytearray = self.socket.recv()
            self.socket.recv()  # Receives Empty Frame
            data: dict = self.socket.recv_json()
            if self.catch_messages:
                self.received_messages.append((id, data))

            self.socket.send(id, zmq.SNDMORE)
            self.socket.send(b"", zmq.SNDMORE)
            if self.use_message_flag:
                self.socket.send_json(self.response_message)
            else:
                self.socket.send_json(data)

    def send_message(self, id: str, message: dict):
        message["is_response"] = False

        self.socket.send(id, zmq.SNDMORE)
        self.socket.send(b"", zmq.SNDMORE)
        self.socket.send_json(message)
