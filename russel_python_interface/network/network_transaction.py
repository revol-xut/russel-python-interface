from crccheck.checksum import Checksum32
from typing import List
from math import ceil

from russel_python_interface.network.network_socket import UDPSocket


class NetworkTransaction:
    udp_socket: UDPSocket = None
    data: bytearray = b""
    size: int = 0
    checksums: List = []
    chunk_size: int = 1024
    uri: str = ""

    @staticmethod
    def create(uri: str, data: bytearray, size: int):
        transaction: NetworkTransaction = NetworkTransaction()
        transaction.udp_socket = UDPSocket.create_server(UDPSocket.uri_too_tuple(uri))
        transaction.data = data
        transaction.size = size
        transaction.uri = uri
        transaction.generate_checksums()

        return transaction

    def generate_checksums(self):
        for i in range(ceil(self.size / self.chunk_size)):
            crc: Checksum32 = Checksum32()
            checksum = crc.process(self.data[i * self.chunk_size: min((i + 1) * self.chunk_size, self.size)]).finalhex()
            self.checksums.append(int(checksum))

    def serialize(self) -> dict:
        return_dict: dict = {
            "size": self.size,
            "chunk_size": self.chunk_size,
            "checksums": self.checksums,
            "uri": self.uri
        }
        return return_dict
