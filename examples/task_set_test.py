import copy
import random
import time
from typing import List, Dict

from russel_python_interface.basic_routines import MatrixScalarProd
from russel_python_interface.engine import Engine
from russel_python_interface.task import Task
from russel_python_interface.task_sets import TaskSet, TaskSetTask

task_count: int = 100
matrix_size: int = 10
time_wait_between_tasks: float = 0.01

user_socket: str = "/run/user/1000/russel.sock"
root_socket: str = "/run/russel.sock"
my_engine: Engine = Engine.create_connect_to_local(user_socket, benchmark=True)
my_engine.upload_all_local_routines()

#my_engine.start()

flop_counter: int = 0
undone_task_counter: int = 0

expecting_task: Dict[str, float] = {}

static_data: Dict[int, List[float]] = {0: [matrix_size], 1: [matrix_size]}
temporary: List[float] = []
for i in range(pow(matrix_size, 2)):
    temporary.append(random.random())
static_data[3] = temporary

task_set: TaskSet = TaskSet.create_task_set(static_data, MatrixScalarProd)
my_engine.register_task_set(task_set)


def task_respond_handler(token: str, task: Task):
    global flop_counter, undone_task_counter, expecting_task
    if token in expecting_task:
        del expecting_task[token]
        undone_task_counter -= 1

    for key in task.response:
        flop_counter += len(task.response[key])


#my_engine.set_task_handler(task_respond_handler)

start_time: float = time.time()
for i in range(task_count):
    new_scalar: float = random.random()

    task: TaskSetTask = TaskSetTask()
    task.my_task_id = task_set.my_task_id
    task.data = [new_scalar]

    token = my_engine.send_task_set_task(task)
    expecting_task[token] = time.time()
    undone_task_counter += 1
    time.sleep(time_wait_between_tasks)
    print("sending", i, my_engine.socket.received_messages)

my_engine.force_schedule()

while len(expecting_task) > 0:

    temp_tokens = copy.deepcopy(expecting_task)

    for task_token in temp_tokens:
        print(task_token, my_engine.socket.received_messages)
        my_engine.resend_task(task_token)
        time.sleep(time_wait_between_tasks)

    my_engine.force_schedule()
    print("Length", len(expecting_task), undone_task_counter)
    time.sleep(0.001)

end_time: float = time.time()

print("Deleting Client Engine ...")
my_engine.delete_task_set(task_set)

my_engine.kill()

print("\n" * 3)
print("========================== Benchmark Results ==========================")

my_engine.benchmark_data.finish_benchmark_response_time()
print(my_engine.benchmark_data)

print("\n========================== External Results ==========================")

print("Time needed:", end_time - start_time)
print("Operations Performed: ", flop_counter)
print("Operations per second", round(flop_counter / (end_time - start_time), 2))

print("\n========================== Configuration ==========================")
print("Matrix Size: ", matrix_size)
print("Task Count: ", task_count)
print("Artificial time delay between task send: ", time_wait_between_tasks)
