import time
from typing import List, Dict

from russel_python_interface.basic_routines import MatrixScalarProd
from russel_python_interface.engine import Engine
from russel_python_interface.methods_file import create_random_task
from russel_python_interface.task import Task

task_count: int = 50
matrix_size: int = 150
time_wait_between_tasks: float = 0.0

my_engine: Engine = Engine.create_connect_to_local("/run/russel.sock", benchmark=True)
my_engine.upload_all_local_routines()

my_engine.start()

start_time: float = time.time()
flop_counter: int = 0

expecting_task: Dict[str, float] = {}


def task_respond_handler(token: str, task: Task):
    global flop_counter
    if token in expecting_task:
        del expecting_task[token]
    else:
        print("Could not find token: ", token)

    for key in task.response:
        flop_counter += len(task.response[key])


my_engine.set_task_handler(task_respond_handler)

for i in range(task_count):
    data: List[float] = create_random_task(matrix_size)
    token: str = my_engine.run_template_task(MatrixScalarProd(), data)
    expecting_task[token] = time.time()
    time.sleep(time_wait_between_tasks)

my_engine.force_schedule()

while len(expecting_task) > 0:
    token = list(expecting_task.keys())[0]
    print(token, my_engine.task_done(token), expecting_task)
    while not my_engine.task_done(token):
        print("length: ", len(expecting_task), " token: ", token)
        if time.time() - expecting_task[token] > 4:
            expecting_task[token] = time.time()
            my_engine.resend_task(token)

        time.sleep(0.1)
        my_engine.force_schedule()
    time.sleep(0.1)

    if token in expecting_task:
        del expecting_task[token]

end_time: float = time.time()

print("\n" * 3)
print("========================== Benchmark Results ==========================")

my_engine.benchmark_data.finish_benchmark_response_time()
print(my_engine.benchmark_data)

print("\n========================== External Results ==========================")

print("Time needed:", end_time - start_time)
print("Operations Performed: ", flop_counter)
print("Operations per second", round(flop_counter / (end_time - start_time), 2))

print("\n========================== Configuration ==========================")
print("Matrix Size: ", matrix_size)
print("Task Count: ", task_count)
print("Artificial time delay between task send: ", time_wait_between_tasks)
