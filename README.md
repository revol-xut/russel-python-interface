Russel - Python - Interface
------------------------------

Russel communication wrapper allows easier integration into other projects.

**Contact**: <revol-xut@protonmail.com>

##Install


```bash
$ pip3 install russel-python-interface
```

##Dependencies

Only some standard python libraries like base64, json

## Unittests

```bash
$ python3 -m unittest discover -v test
```

##Examples

See the [Russel-Benchmark-Repository](https://bitbucket.org/revol-xut/russel-benchmarks/) for examples.
 
##Compatibility
 
Compatible with Russel Version **v0.1f** and higher

## Change Log

**v0.1b**(2020.x.x)

**v0.1a** (2020.7.2)

+ Pypi Configuration
+ Added internal Benchmarks
+ Added Task, Routine, Template, Socket and Engine Classes as
+ Other core functionalities
